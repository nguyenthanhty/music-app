//
//$(function () {
//   var images = ['1.jpg','2.jpg'],
//    index = 0, // starting index
//    maxImages = images.length - 1;
//setInterval(function() {
//    var currentImage = images[index];
//    index = (index == maxImages) ? 0 : ++index;
//    $('#img-be-changed').fadeOut(200, function() {
//        $('#img-be-changed').attr("src", '/images/slide/'+currentImage);
//        $('#img-be-changed').fadeIn("slow");
//    });
// }, 5000); 
//})
//




$(function () {
    // animate on scroll
    wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: true
    })
    wow.init(); 
});

$(function () {
    $('.first-item').addClass('home-menu');
    $(".menu-item").click(function () {
        $(".menu-item").removeClass("highlight")
        $(".menu-item").removeClass("home-menu")
        $(this).addClass("highlight")
    });

    (function ($) {
        var $window = $(window),
            $html = $('#player-outer');

        $window.resize(function resize() {
            if ($window.width() >= 550 && $window.width() <= 991) {
                $html.removeClass('container');
                return $html.addClass('container-fluid');
            } else {
                $html.removeClass('container-fluid');
                return $html.addClass('container');
            }
        }).trigger('resize');
    })(jQuery);
});


var audio;

//Hide Pause
$('#pause').hide();

initAudio($('#playlist li:first-child'));

function initAudio(element) {
  var song = element.attr('song');
  var title = element.text();
  var cover = element.attr('cover');
  var artist = element.attr('artist');

  //Create audio object
  audio = new Audio('media/' + song);

  //Insert audio info
  $('.artist').text(artist);
  $('.title').text(title);

  //Insert song cover

  $('#playlist li').removeClass('active');
  element.addClass('active');
}

//Play button
$('#play').click(function() {
  audio.play();
  $('#play').hide();
  $('#pause').show();
  showDuration();
});

//Pause button
$('#pause').click(function() {
  audio.pause();
  $('#play').show();
  $('#pause').hide();
});

//Next button
$('#next').click(function() {
  audio.pause();
  var next = $('#playlist li.active').next();
  if (next.length == 0) {
    next = $('#playlist li:first-child');
  }
  initAudio(next);
  audio.play();
  $('#pause').show();
  $('#play').hide();
  showDuration();
});

//Prev button
$('#prev').click(function() {
  audio.pause();
  var prev = $('#playlist li.active').prev();
  if (prev.length == 0) {
    prev = $('#playlist li:last-child');
  }
  initAudio(prev);
  audio.play();
  $('#pause').show();
  $('#play').hide();
  showDuration();
});

//Playlist song click
$('#playlist li').click(function() {
  audio.pause();
  initAudio($(this));
  $('#play').hide();
  $('#pause').show();
  audio.play();
  showDuration();
});

//Volume control
$('#volume').change(function() {
  // audio.volume = parseFloat(this.value / 10);
  audio.volume = parseInt(this.value) / 10;
  step = .01;
  min = 0;
  max = 1;
  value = 1;
});

//Time/Duration
function showDuration() {
  $(audio).bind('timeupdate', function() {
    //Get hours and minutes
    var s = parseInt(audio.currentTime % 60);
    var m = parseInt(audio.currentTime / 60) % 60;
    if (s < 10) {
      s = '0' + s;
    }
    $('#duration').html('0' + m + ':' + s);
    var value = 0;
    if (audio.currentTime > 0) {
      value = Math.floor((100 / audio.duration) * audio.currentTime);
    }
    $('#progress').css('width', value + '%');
  });
}

var VolumeSlider = player.querySelector('.volume-bar');
VolumeSlider.addEventListener('input', function() {
  audio.volume = parseInt(this.value) / 10;
  step = .01;
  min = 0;
  max = 1;
  value = 1;
}, false);

/*=================================================
                Change introduction image 
==================================================*/
